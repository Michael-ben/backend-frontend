import { IsNumber, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class QueryOptions {
    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    readonly page?: number;

    @IsNumber()
    @Type(() => Number)
    @IsOptional()
    readonly limit?: number;

    @IsString()
    @IsOptional()
    readonly q?: string = '';
}
