
export function generateUniqueCode(prefix) {
    let codeProspect = prefix + Math.random().toString(16).substr(2, 5);
    codeProspect += (new Date()).getFullYear();

    return codeProspect;
}
