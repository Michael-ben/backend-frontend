import { HttpModule, Module } from '@nestjs/common';
import { Environments } from './environments';
import { DatabaseExceptionFilter } from './exceptions';
import { MongooseModule } from './mongoose/mongoose.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { UserProvider } from '../app/user/provider/user.provider';
import { ProfilProvider } from '../app/user/provider/profil.provider';
import { EntityProvider } from '../app/user/provider/entity.provider';
import { ProfilService } from '../app/user/service/profil.service';
import { EntityService } from '../app/user/service/entity.service';
import { AuthService } from '../app/auth/service/auth.service';
import { UserService } from '../app/user/service/user.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../app/auth/constants';

@Module({
    imports: [
        MongooseModule,
        HttpModule,
        MailerModule.forRoot({
            transport: {
                host: 'email-smtp.us-west-2.amazonaws.com',
                port: 587,
                secure: false,
                requireTLS: true,
                auth: {
                    user: 'AKIAJ4NKT5QON3OZ5XFA',
                    pass: 'AqhOPnqQcG1MQ3R3+L3uHlt+UMHNNXHUKnUIk63yWZv/',
                },
            },
            defaults: {
                from: 'No "Reply" <no-reply@localhost>',
            },
            template: {
                dir: process.cwd() + '/templates/',
                options: {
                    strict: true,
                },
            },
        }),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
    ],
    controllers: [],
    providers: [
        DatabaseExceptionFilter,
        Environments,
        ...UserProvider,
        ...ProfilProvider,
        ...EntityProvider,
        /* ...forgotPasswordProvider, */
        UserService,
        AuthService,
        ProfilService,
        EntityService,
    ],
    exports: [
        DatabaseExceptionFilter,
        Environments,
        MongooseModule,
        ...EntityProvider,
        ...UserProvider,
        ...ProfilProvider,
        /*    ...forgotPasswordProvider, */
        UserService,
        EntityService,
        ProfilService,
        AuthService,

        MailerModule.forRoot({
            transport: {
                host: process.env.MAILER_HOST,
                port: Number(process.env.MAILER_PORT),
                secure: false,
                requireTLS: true,
            },
            defaults: {
                from: 'No "Reply" <noreply@akilcab.com>',
            },
            template: {
                dir: process.cwd() + '/templates/',
                options: {
                    strict: true,
                },
            },
        }),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
        HttpModule,
    ],
})
export class SharedModule {}
