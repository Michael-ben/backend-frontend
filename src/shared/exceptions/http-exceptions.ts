import { HttpStatus, HttpException } from '@nestjs/common';

/**
 * 400: Bad Request
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/400
 *
 * @export
 * @class BadRequestException
 * @extends {HttpException}
 */
export class BadRequestException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                ok: null,
                data: null,
                message: msg,
                status_code: HttpStatus.BAD_REQUEST,
                meta: [],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}

/**
 * 401: Unauthorized
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/401
 *
 * @export
 * @class UnauthorizedException
 * @extends {HttpException}
 */
export class UnauthorizedException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.UNAUTHORIZED,
                meta: [],
            },
            HttpStatus.UNAUTHORIZED,
        );
    }
}

/**
 * 403: Forbidden
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/403
 *
 * @export
 * @class ForbiddenException
 * @extends {HttpException}
 */
export class ForbiddenException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.FORBIDDEN,
                meta: [],
            },
            HttpStatus.FORBIDDEN,
        );
    }
}

/**
 * 404: Not Found
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/404
 *
 * @export
 * @class NotFoundException
 * @extends {HttpException}
 */
export class NotFoundException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.NOT_FOUND,
                meta: [],
            },
            HttpStatus.NOT_FOUND,
        );
    }
}

/**
 * 405: Method Not Allowed
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/405
 *
 * @export
 * @class NotAllowedException
 * @extends {HttpException}
 */
export class NotAllowedException extends HttpException {
    constructor(msg: string | object = 'Method Not Allowed') {
        super(
            {
                data: null,
                message: msg,
                status_code: 405,
                meta: [],
            },
            405,
        );
    }
}

/**
 * 409: Conflict
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/409
 *
 * @export
 * @class ConflictException
 * @extends {HttpException}
 */
export class ConflictException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.CONFLICT,
                meta: [],
            },
            HttpStatus.CONFLICT,
        );
    }
}

/**
 * 410: Gone
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/410
 *
 * @export
 * @class GoneException
 * @extends {HttpException}
 */
export class GoneException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.GONE,
                meta: [],
            },
            HttpStatus.GONE,
        );
    }
}

/**
 * 415: Unsupported Media Type
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/415
 *
 * @export
 * @class UnsupportedMediaException
 * @extends {HttpException}
 */
export class UnsupportedMediaException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                meta: [],
            },
            HttpStatus.UNSUPPORTED_MEDIA_TYPE,
        );
    }
}

/**
 * 429: Too Many Requests
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/429
 *
 * @export
 * @class TooManyRequestsException
 * @extends {HttpException}
 */
export class TooManyRequestsException extends HttpException {
    constructor(msg: string | object) {
        super(
            {
                data: null,
                message: msg,
                status_code: HttpStatus.TOO_MANY_REQUESTS,
                meta: [],
            },
            HttpStatus.TOO_MANY_REQUESTS,
        );
    }
}
