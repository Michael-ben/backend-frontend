import {
    BadRequestException,
    UnauthorizedException,
    ForbiddenException,
    NotFoundException,
    NotAllowedException,
    ConflictException,
    GoneException,
    UnsupportedMediaException,
    TooManyRequestsException,
} from './http-exceptions';

describe('HTTP Exceptions', () => {
    test('BAD_REQUEST: Should have the correct properties', () => {
        const exception = new BadRequestException('message');
        expect(exception.getStatus()).toBe(400);
    });

    test('UNAUTHORIZED: Should have the correct properties', () => {
        const exception = new UnauthorizedException('message');
        expect(exception.getStatus()).toBe(401);
    });

    test('FORBIDDEN: Should have the correct properties', () => {
        const exception = new ForbiddenException('message');
        expect(exception.getStatus()).toBe(403);
    });

    test('NOT_FOUND: Should have the correct properties', () => {
        const exception = new NotFoundException('message');
        expect(exception.getStatus()).toBe(404);
    });

    test('NOT_ALLOWED: Should have the correct properties', () => {
        const exception = new NotAllowedException('message');
        expect(exception.getStatus()).toBe(405);
    });

    test('CONFLICT: Should have the correct properties', () => {
        const exception = new ConflictException('message');
        expect(exception.getStatus()).toBe(409);
    });

    test('GONE: Should have the correct properties', () => {
        const exception = new GoneException('message');
        expect(exception.getStatus()).toBe(410);
    });

    test('UNSUPPORTED_MEDIA_TYPE: Should have the correct properties', () => {
        const exception = new UnsupportedMediaException('message');
        expect(exception.getStatus()).toBe(415);
    });

    test('UNSUPPORTED_MEDIA_TYPE: Should have the correct properties', () => {
        const exception = new TooManyRequestsException('message');
        expect(exception.getStatus()).toBe(429);
    });
});
