import * as mongoose from 'mongoose';
import {DB_PROVIDER} from '../constants';
import {MongooseConfig} from './mongoose.confg';
import * as mongoosePaginate from 'mongoose-paginate';


export const MongooseProviders = [
    {
        provide: DB_PROVIDER,
        useFactory: async () => {
            (mongoose as any).Promise = global.Promise;
            const mongooseConfig: MongooseConfig = new MongooseConfig();

            mongoose.plugin(mongoosePaginate);

            return await mongoose.connect(
                mongooseConfig.configure(),
                {
                    useCreateIndex: true,
                    useNewUrlParser: true,
                    useUnifiedTopology: true,
                    useFindAndModify: false,
                    ignoreUndefined: true
                });
        },
    },
];
