import { Module } from '@nestjs/common';
import { MongooseProviders } from './mongoose.provider';

@Module({
    controllers: [],
    providers: [...MongooseProviders],
    exports: [...MongooseProviders],
})
export class MongooseModule {}
