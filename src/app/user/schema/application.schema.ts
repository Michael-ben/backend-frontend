import * as mongoose from 'mongoose';

export const ApplicationSchema = new mongoose.Schema(
  {
    app_name: { type: String, trim: true },
    app_code: { type: String, trim: true },
    
    entity: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Entity'
    },
    created_by: { type: String, trim: true },
    updated_by: { type: String, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  },
);