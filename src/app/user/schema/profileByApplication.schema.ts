import * as mongoose from 'mongoose';
export const ProfilByApplicationSchema = new mongoose.Schema(
  {

    profil: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Profil'
    },
    application: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Application'
    },
    
    created_by: { type: String, trim: true },
    updated_by: { type: String, trim: true },
  },
  {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  },
);