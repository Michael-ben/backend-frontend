import * as mongoose from 'mongoose';
import validator from 'validator';

export const EntitySchema = new mongoose.Schema(
    {
        first_name: { type: String, trim: true },
        last_name: { type: String, trim: true },
        email: {
            type: String,
            required: [true, "L'email est requis"],
            validate: [validator.isEmail, "L'email est invalid"],
            isAsync: false,
            trim: true,
            index: true,
            unique: true,
        },

        company_name: {
            type: String,
            trim: true,
            unique: true,
            index: true,
            sparse: true,
        },
        created_by: { type: String, trim: true },
        updated_by: { type: String, trim: true },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    },
);
