import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Query } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateProfilByApplicationDto } from '../dto/ProfilByApplication.dto';
import { ProfilByApplicationService } from '../service/profilByApplication.service';
import { QueryOptions } from '../../../shared/utils/query-options';
@ApiTags('ProfilByApplication')
  @Controller('ProfilByApplication')
export class ProfilByApplicationController {
  constructor(private readonly ProfilByApplicationService: ProfilByApplicationService
  ) { }
  @Get()
  public async getProfilByApplication(@Query() options: QueryOptions) {
    return {
      data: await this.ProfilByApplicationService.getAll(options),
      message: `success`,
      status_code: HttpStatus.OK,
      meta:[]
    };
  }
  @ApiOperation({})
  @Get('Id')
  public async getAcces(@Param('Id')Id: string) {
    const ProfilByApplication = await this.ProfilByApplicationService.getById(Id);
    return {
      message: `liste des profils d'applications`,
      ...ProfilByApplication,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }
  @ApiOperation({})
  @Post('/profil/:profilId/application/:aplicationId')
  public async PostCreate(
    @Param('applicationId') applicationId: string,
    @Param('profilId') profilId: string,
    @Body() createProfilByApplicationDto: CreateProfilByApplicationDto,
  ) {
    return {
      data: await this.ProfilByApplicationService.create(
        profilId, applicationId,
        createProfilByApplicationDto
      ),
      message: ` créer avec succes`,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }

  @ApiOperation({})
  @Delete('delete')
  async delete(
    @Query('Id') Id: string
  ) {
    return {
      data: await this.ProfilByApplicationService.delete(
        Id

      ),
      message: ` supprimé avec succes`,
      status_code: HttpStatus.OK,
      meta: [],
    }

  }

  @ApiOperation({})
  @Put(':Id')
  public async PostUpdate(
    @Param('Id') Id: string,
    @Body() UpdateProfilByApplicationDto: CreateProfilByApplicationDto
  ) {
    return {
      data: await this.ProfilByApplicationService.update(
        Id, UpdateProfilByApplicationDto
      ),
      message: ` renommée avec succes`,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }


}