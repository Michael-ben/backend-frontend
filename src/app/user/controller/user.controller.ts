import {Body,Controller,Delete,Get,HttpStatus,Param,Post,Put,Query} from '@nestjs/common';
import { UserService } from '../service/user.service';
import { ApiTags, ApiOperation, ApiCreatedResponse, ApiBody } from '@nestjs/swagger';
import { createUserDto } from '../dto/createUser.dto';
import { QueryOptions } from '../../../shared/utils/query-options';
import { UpdateUserAccessDto } from '../dto/update-user-access.dto';
@ApiTags('Users / Collaborateurs')
@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}
    @ApiOperation({})
    @Get()
    @ApiCreatedResponse({ description: 'list all users' })
    public async getUsers(@Query() options: QueryOptions) {
        return {
            data: await this.userService.getAll(options),
            message: `success`,
            status_code: HttpStatus.OK,
            meta:[],
        };
    }
/*     @ApiOperation({})
    @Get('/confirmation-tokens/:token')
    @ApiCreatedResponse({ description: 'user token' })
    public async getUserAccess(@Param('token') token: string) {
        return {
            data: await this.userService.validateToken(token),
            message: `success`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    } */
    @ApiOperation({})
    @Get(':Id')
    @ApiCreatedResponse({ description: 'list user by Id' })
    public async getUserById(@Param('Id') Id: string) {
        return {
            data: await this.userService.getById(Id),
            message: `success`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
    @ApiOperation({})
    @Post('')
    @ApiCreatedResponse({ description: 'user Registration' })
    public async createUser(@Body() createUserDto: createUserDto) {
        return {
            data: await this.userService.createUser(createUserDto),
            message: `created`,
            status_code: HttpStatus.CREATED,
            meta: [],
        };
    }

/*     @ApiOperation({})
    @Put('/confirmation-tokens/:token')
    @ApiCreatedResponse({ description: 'user information modification' })
    public async putUserAccess(
        @Param('token') token: string,
        @Body() updateUserAccessDto: UpdateUserAccessDto,
    ) {
        return {
            data: await this.userService.validateUserAccess(
                token,
                updateUserAccessDto,
            ),
            message: `updated`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    } */
    @ApiOperation({})
    @Delete('/delete')
    @ApiCreatedResponse({ description: 'delete all information for user' })
    async deleteUser(@Query('userId') userId) {
        return {
            data: await this.userService.deleteUser(userId),
            message: `deleted`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
    @ApiOperation({})
    @Put('/:id')
    @ApiCreatedResponse({ description: 'user information modification' })
    public async putUser(
        @Param('id') id: string,
        @Body() createUserDto: createUserDto,
    ) {
        return {
            data: await this.userService.Update(id, createUserDto),
            message: `updated`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
}
