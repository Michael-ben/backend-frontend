import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Query } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { CreateApplicationDto} from '../dto/Application.dto';
import { ApplicationService } from '../service/application.service';
import { QueryOptions } from '../../../shared/utils/query-options';
@ApiTags('applications')
@Controller('applications')
export class ApplicationController {
  constructor(private readonly ApplicationService: ApplicationService
  ) {}
  @Get()
  public async getApplications(@Query() options: QueryOptions) {
    return {
      data: await this.ApplicationService.getAll(options),
      message: `success`,
      status_code: HttpStatus.OK,
      meta:[],
    };
  }
  @ApiOperation({})
  @Get(':Id')
  public async getApplication(@Param('Id') Id: string) {

    return {
      data: await this.ApplicationService.getById(Id),
      message: `liste de vos applications`,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }
  @ApiOperation({})
  @Post('entity/:Id')
  public async Create(
    @Param('Id') entiteId: string,
    @Body() createApplicationDto: CreateApplicationDto,
  ) {
    return {
      data: await this.ApplicationService.createApplication(
         entiteId,
        createApplicationDto
      ),
      message: `application créer avec succes`,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }

  @ApiOperation({})
  @Delete('delete')
  async delete(
    @Query('applicationId') applicationId: string
  ) {
    return {
        data: await this.ApplicationService.delete(applicationId),
        message: `application supprimé avec succes`,
        status_code: HttpStatus.OK,
        meta: [],
    };

  }

  @ApiOperation({})
  @Put('/:Id')
  public async PostUpdate(
    @Param('Id') Id: string,
    @Body() UpdateApplicationDto: CreateApplicationDto
  ) {
    return {
      data: await this.ApplicationService.updateApplication(
        Id, UpdateApplicationDto
      ),
      message: `application renommée avec succes`,
      status_code: HttpStatus.OK,
      meta: [],
    };
  }
}