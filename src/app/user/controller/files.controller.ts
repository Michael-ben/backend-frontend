import { Post, Get, Controller, UseInterceptors, UploadedFiles, HttpException, HttpStatus, Query, Param, Res } from '@nestjs/common';
import { ApiConsumes, ApiBadRequestResponse, ApiTags, ApiCreatedResponse} from '@nestjs/swagger';
import { FilesInterceptor } from '@nestjs/platform-express';
import { FilesService } from '../service/files.service';
import { FileResponseVm } from '../interface/file-response-vm.model'
import { FileInfoVm } from '../interface/file-info-vm.model'
import { GetFilesFilterDto } from '../dto/GetFile.dto'

@Controller('/attachment/files')
@ApiTags('Attachments')
export class FilesController {
  constructor(private filesService: FilesService) { }
  @Post('')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('file'))
  upload(@UploadedFiles() files) {
    const response = [];
    files.forEach(file => {
      const fileReponse = {
        originalname: file.originalname,
        encoding: file.encoding,
        mimetype: file.mimetype,
        id: file.id,
        filename: file.filename,
        metadata: file.metadata,
        bucketName: file.bucketName,
        chunkSize: file.chunkSize,
        size: file.size,
        md5: file.md5,
        uploadDate: file.uploadDate,
        contentType: file.contentType,
      };
      response.push(fileReponse);
    });
    return response;
  }

  @Get(':filter')
  @ApiBadRequestResponse({})
  async getFiles(@Query('') filterDto: GetFilesFilterDto, @Query('') filter): Promise<FileInfoVm[]> {
    const files = await this.filesService.getAll(filter)
    if (Object.keys(GetFilesFilterDto ).length){
      return this.filesService.getFileByFilename(filterDto)
    }
    else{
      return {
        
        ...files
      }
    }

  }

    


  @Get('info/:id')
  @ApiBadRequestResponse({  })
  async getFileInfo(@Param('id') id: string): Promise<FileResponseVm> {
    const file = await this.filesService.findInfo(id)
    const filestream = await this.filesService.readStream(id)
    if (!filestream) {
      throw new HttpException('An error occurred while retrieving file info', HttpStatus.EXPECTATION_FAILED)
    }
    return {
      message: 'File has been detected',
      file: file
    }
  }

  @Get(':id')
  @ApiBadRequestResponse({  })
  async getFile(@Param('id') id: string, @Res() res) {
    const file = await this.filesService.findInfo(id)
    const filestream = await this.filesService.readStream(id)
    if (!filestream) {
      throw new HttpException('An error occurred while retrieving file', HttpStatus.EXPECTATION_FAILED)
    }
    res.header('Content-Type', file.contentType);
    return filestream.pipe(res)
  }

  @Get('download/:id')
  @ApiBadRequestResponse({  })
  async downloadFile(@Param('id') id: string, @Res() res) {
    const file = await this.filesService.findInfo(id)
    const filestream = await this.filesService.readStream(id)
    if (!filestream) {
      throw new HttpException('An error occurred while retrieving file', HttpStatus.EXPECTATION_FAILED)
    }
    res.header('Content-Type', file.contentType);
    res.header('Content-Disposition', 'attachment; filename=' + file.filename);
    return filestream.pipe(res)
  }

  @Get('delete/:id')
  @ApiBadRequestResponse({  })
  @ApiCreatedResponse({ type: FileResponseVm })
  async deleteFile(@Param('id') id: string): Promise<FileResponseVm> {
    const file = await this.filesService.findInfo(id)
    const filestream = await this.filesService.deleteFile(id)
    if (!filestream) {
      throw new HttpException('An error occurred during file deletion', HttpStatus.EXPECTATION_FAILED)
    }
    return {
      message: 'File has been deleted',
      file: file
    }
  }
}