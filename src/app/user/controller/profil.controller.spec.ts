import { ProfilController } from './profil.controller';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SharedModule } from '../../../shared/shared.module';
import { ProfilProvider } from '../provider/profil.provider';
import { ProfilService } from '../service/profil.service';
import { PROFIL_MODEL } from '../constants';

expect.extend({
    toMatchOneOf,
    toMatchShapeOf,
});
const data = {
    is_first_user: true,
    secret: 'secretkey',
    key: 'test@',
};
const userId = '12566d6fmfmfff';
const entiteId = 'ddkksdks6s6s66s';

describe('Profil Controller', () => {
    let controller: ProfilController;
    let modelProfil;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ProfilController],
            providers: [...ProfilProvider, ProfilService],
            imports: [SharedModule],
        }).compile();

        controller = module.get<ProfilController>(ProfilController);
        modelProfil = module.get(PROFIL_MODEL);
    });

    describe('Create', () => {
        it('It should create a new  user profil', async () => {
            const profil = await new modelProfil(data);
            const expectedResponse = {
                data: profil,
                message: 'created',
                status_code: HttpStatus.CREATED,
                meta: [],
            };
            jest.spyOn(controller, 'PostCreate').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.PostCreate(
                userId,
                entiteId,
                data,
            );
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
    describe('ProfilUpdate', () => {
        it('It should update a entreprise entity', async () => {
            const profil = await new modelProfil(data);
            const updateData = { ...profil, secret: 'MME' };
            const expectedResponse = {
                data: updateData,
                meta: [],
                message: 'success',
                status_code: HttpStatus.OK,
            };
            jest.spyOn(controller, 'PostUpdate').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.PostUpdate(profil._Id, data);
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
    describe('getProfil', () => {
        it('It should get one profil', async () => {
            const profil = await new modelProfil(data);
            const expectedResponse = {
                data: profil,
                message: 'success',
                status_code: HttpStatus.OK,
                meta: [],
            };
            jest.spyOn(controller, 'getProfil').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.getProfil(userId, entiteId);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
    describe('getAll', () => {
        it('should successful list all profils', async () => {
            const options = { page: 1, limit: 0 };
            const expectedResponse = {
                data: [],
                meta: {
                    current_page: 1,
                    first_page: 1,
                    last_page: 1,
                    total: 1,
                },
                message: 'success',
                status_code: HttpStatus.OK,
            };
            const response = await controller.getProfils(options);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
    describe('deleteProfil', () => {
        it('It should delete one profil', async () => {
            const profil = await new modelProfil(data);
            const expectedResponse = {
                data: null,
                meta: [],
                message: 'deleted',
                status_code: HttpStatus.OK,
            };
            jest.spyOn(controller, 'delete').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.delete(profil._id);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
});
