/* eslint-disable @typescript-eslint/no-empty-function */
import { UserController } from './user.controller';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SharedModule } from '../../../shared/shared.module';
import { UserProvider } from '../provider/user.provider';
import { UserService } from '../service/user.service';
import { USER_MODEL } from '../constants';

expect.extend({
    toMatchOneOf,
    toMatchShapeOf,
});
const data = {
    first_name: 'first_name',
    last_name: 'last_name',
    email: 'test@gmail.com',
};
const id = '1fd2d36f6fkfkfkfvdld';
describe('User Controller', () => {
    let controller: UserController;
    let modelUser;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [UserController],
            providers: [...UserProvider, UserService],
            imports: [SharedModule],
        }).compile();

        controller = module.get<UserController>(UserController);
        modelUser = module.get(USER_MODEL);
    });

    describe('Create', () => {
        it('It should create a new particular user', async () => {
            const user = await new modelUser(data);
            const expectedResponse = {
                data: user,
                message: 'created',
                status_code: HttpStatus.CREATED,
                meta: [],
            };
            jest.spyOn(controller, 'createUser').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.createUser(data);
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });

    describe('putUserAccess', () => {
        it('It should create a new user access', async () => {});
    });
    describe('updateUser', () => {
        it('It should update a entreprise entity', async () => {
            const entreprise = await new modelUser(data);
            const updateData = { ...entreprise, last_name: 'MME' };
            const expectedResponse = {
                data: updateData,
                meta: [],
                message: 'success',
                status_code: HttpStatus.OK,
            };
            jest.spyOn(controller, 'putUser').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.putUser(entreprise._Id, data);
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
    describe('getUserById', () => {
        it('It should get one entity', async () => {
            const user = await new modelUser(data);
            const expectedResponse = {
                data: user,
                message: 'success',
                status_code: HttpStatus.OK,
                meta: [],
            };
            jest.spyOn(controller, 'getUserById').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.getUserById(id);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
    describe('getAll', () => {
        it('should successful list all users', async () => {
            const options = { page: 1, limit: 0 };
            const expectedResponse = {
                data: [],
                meta: {
                    current_page: 1,
                    first_page: 1,
                    last_page: 1,
                    total: 1,
                },
                message: 'success',
                status_code: HttpStatus.OK,
            };
            const response = await controller.getUsers(options);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
});
