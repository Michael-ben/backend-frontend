/* eslint-disable @typescript-eslint/no-unused-vars */
import { Body, Controller, HttpStatus, Post, Param, Put,Get,Query } from '@nestjs/common';
import { EntityService } from '../service/entity.service';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { registerEntityDto } from '../dto/registerEntity.dto';
import { QueryOptions } from '../../../shared/utils/query-options';
@ApiTags('Entities / Collaborateurs')
@Controller('entities')
export class EntityController {
    constructor(private readonly EntityService: EntityService) {}
    @ApiOperation({})
    @Get()
    public async getEntities(@Query() options: QueryOptions) { 
        return {
            data: await this.EntityService.getAll(options),
            message: `success`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }

    @ApiOperation({})
    @Get(':Id')
    public async getEntityById(@Param('Id') Id: string) {
        return {
            data: await this.EntityService.getById(Id),
            message: `success`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
    @Post('')
    public async Create(
        @Body() createEntityDto: registerEntityDto,
    ) {
        return {
            data: await this.EntityService.createEntity(
                createEntityDto,
            ),
            message: `created`,
            status_code: HttpStatus.CREATED,
            meta: [],
        };
    }
    
    @ApiOperation({})
    @Put('/:id')
    public async putEntity(
        @Param('userId') userId: string,
        @Body() updateEntrepriseDto: registerEntityDto,
    ) {
        return {
            data: await this.EntityService.updateEntity(
                userId,
                updateEntrepriseDto,
            ),
            message: `updated`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
}
