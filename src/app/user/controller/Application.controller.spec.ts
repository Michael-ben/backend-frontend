import { ApplicationController } from './Application.controller';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SharedModule } from '../../../shared/shared.module';
import { ApplicationProvider } from '../provider/application.provider';
import { ApplicationService } from '../service/application.service';
import { APPLICATION_MODEL } from '../constants';

expect.extend({
  toMatchOneOf,
  toMatchShapeOf,
});
const data = {
  app_name: 'first_name',
  app_code: 'last_name',
  
};
const id = '1fd2d36f6fkfkfkfvdld';
describe('Application Controller', () => {
  let controller: ApplicationController;
  let modelApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApplicationController],
      providers: [...ApplicationProvider, ApplicationService],
      imports: [SharedModule],
    }).compile();

    controller = module.get<ApplicationController>(ApplicationController);
    modelApplication = module.get(APPLICATION_MODEL);
  });
  describe('ApplicationCreate', () => {
    it('It should create a new application', async () => {
      const application = await new modelApplication(data);
      const expectedResponse = {
        data: application,
        message: 'created',
        status_code: HttpStatus.CREATED,
        meta: [],
      };
      jest.spyOn(controller, 'Create ').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.Create(id , application);
      expect(response).toMatchShapeOf(expectedResponse);
    }, 99999);
  });
  describe('updateApplication', () => {
    it('It should update an application', async () => {
      const application = await new modelApplication(data);
      const updateData = { ...application, app_name: 'MME' };
      const expectedResponse = {
        data: updateData,
        meta: [],
        message: 'success',
        status_code: HttpStatus.OK,
      };
      jest.spyOn(controller, 'PostUpdate').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.PostUpdate(
        application._Id,
        data,
      );
      expect(response).toMatchShapeOf(expectedResponse);
    }, 99999);
  });
  describe('getApplicationById', () => {
    it('It should get one application', async () => {
      const application = await new modelApplication(data);
      const expectedResponse = {
        data: application,
        message: 'success',
        status_code: HttpStatus.OK,
        meta: [],
      };
      jest.spyOn(controller, 'getApplication').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.getApplication(id);
      expect(response).toMatchShapeOf(expectedResponse);
    });
  });
  describe('getAll', () => {
    it('should successful list all applications', async () => {
      const options = { page: 1, limit: 0 };
      const expectedResponse = {
        data: [],
        meta: {
          current_page: 1,
          first_page: 1,
          last_page: 1,
          total: 1,
        },
        message: 'success',
        status_code: HttpStatus.OK,
      };
      const response = await controller.getApplications(options);
      expect(response).toMatchShapeOf(expectedResponse);
    });
  });
});
