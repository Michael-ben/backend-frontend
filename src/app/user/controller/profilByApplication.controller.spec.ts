import { ProfilByApplicationController } from './profilByapplication.controller';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SharedModule } from '../../../shared/shared.module';
import { ProfilByApplicationProvider } from '../provider/ProfilByApplication.provider';
import { ProfilByApplicationService } from '../service/ProfilByApplication.service';
import { PROFILBYAPPLICATION_MODEL } from '../constants';

expect.extend({
  toMatchOneOf,
  toMatchShapeOf,
});
const data = {
  email: 'test@gmail.com',
};
const id = '1fd2d36f6fkfkfkfvdld';
describe('ProfilByApplication Controller', () => {
  let controller: ProfilByApplicationController;
  let modelProfilByApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProfilByApplicationController],
      providers: [...ProfilByApplicationProvider, ProfilByApplicationService],
      imports: [SharedModule],
    }).compile();

    controller = module.get<ProfilByApplicationController>(ProfilByApplicationController);
    modelProfilByApplication = module.get(PROFILBYAPPLICATION_MODEL);
  });
  describe('ProfilByApplicationCreate', () => {
    it('It should create a new profil application', async () => {
      const profilByApplication = await new modelProfilByApplication(data);
      const expectedResponse = {
        data: profilByApplication,
        message: 'created',
        status_code: HttpStatus.CREATED,
        meta: [],
      };
      jest.spyOn(controller, 'ProfilByApplication').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.ProfilByApplicationCreate(profilByApplication);
      expect(response).toMatchShapeOf(expectedResponse);
    }, 99999);
  });
  describe('ProfilByApplicationUpdate', () => {
    it('It should update a profil application', async () => {
      const profilByApplication = await new modelProfilByApplication(data);
      const updateData = { ...profilByApplication, can_send_sms: 'false' };
      const expectedResponse = {
        data: updateData,
        meta: [],
        message: 'success',
        status_code: HttpStatus.OK,
      };
      jest.spyOn(controller, 'putProfilByApplication').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.PostUpdate(
        profilByApplication._Id,
        data,
      );9-
      expect(response).toMatchShapeOf(expectedResponse);
    }, 99999);
  });
  describe('getApplicationById', () => {
    it('It should get one entity', async () => {
      const profilByApplication = await new modelProfilByApplication(data);
      const expectedResponse = {
        data: profilByApplication,
        message: 'success',
        status_code: HttpStatus.OK,
        meta: [],
      };
      jest.spyOn(controller, 'getprofilByApplicationById').mockResolvedValue(
        expectedResponse,
      );
      const response = await controller.get(id);
      expect(response).toMatchShapeOf(expectedResponse);
    });
  });
  describe('getProfilByApplication', () => {
    it('should successful list all ProfilByApplication', async () => {
      const options = { page: 1, limit: 0 };
      const expectedResponse = {
        data: [],
        meta: {
          current_page: 1,
          first_page: 1,
          last_page: 1,
          total: 1,
        },
        message: 'success',
        status_code: HttpStatus.OK,
      };
      const response = await controller.getProfilByApplication(options);
      expect(response).toMatchShapeOf(expectedResponse);
    });
  });
});
