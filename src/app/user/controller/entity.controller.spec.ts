import { EntityController } from './entity.controller';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SharedModule } from '../../../shared/shared.module';
import { EntityProvider } from '../provider/entity.provider';
import { EntityService } from '../service/entity.service';
import { ENTITY_MODEL } from '../constants';

expect.extend({
    toMatchOneOf,
    toMatchShapeOf,
});
const data = {
    manager_first_name: 'first_name',
    manager_last_name: 'last_name',
    company_name: 'akil',
    email: 'test@gmail.com',
};
const id = '1fd2d36f6fkfkfkfvdld';
describe('Entity Controller', () => {
    let controller: EntityController;
    let modelEntity;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EntityController],
            providers: [...EntityProvider, EntityService],
            imports: [SharedModule],
        }).compile();

        controller = module.get<EntityController>(EntityController);
        modelEntity = module.get(ENTITY_MODEL);
    });
    describe('EntityCreate', () => {
        it('It should create a new entity', async () => {
            const entity = await new modelEntity(data);
            const expectedResponse = {
                data: entity,
                message: 'created',
                status_code: HttpStatus.CREATED,
                meta: [],
            };
            jest.spyOn(controller, 'Create').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.Create(entity);
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
    describe('EntityUpdate', () => {
        it('It should update a specific entity', async () => {
            const entreprise = await new modelEntity(data);
            const updateData = { ...entreprise, company_name: 'MME' };
            const expectedResponse = {
                data: updateData,
                meta: [],
                message: 'success',
                status_code: HttpStatus.OK,
            };
            jest.spyOn(controller, 'putEntity').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.putEntity(
                entreprise._Id,
                data,
            );
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
    describe('getEntityById', () => {
        it('It should get one entity', async () => {
            const entity = await new modelEntity(data);
            const expectedResponse = {
                data: entity,
                message: 'success',
                status_code: HttpStatus.OK,
                meta: [],
            };
            jest.spyOn(controller, 'getEntityById').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.getEntityById(id);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
    describe('getAll', () => {
        it('should successful list all entities', async () => {
            const options = { page: 1, limit: 0 };
            const expectedResponse = {
                data: [],
                meta: {
                    current_page: 1,
                    first_page: 1,
                    last_page: 1,
                    total: 1,
                },
                message: 'success',
                status_code: HttpStatus.OK,
            };
            const response = await controller.getEntities(options);
            expect(response).toMatchShapeOf(expectedResponse);
        });
    });
});
