import {Body,Controller, Delete,Get,HttpStatus,Param,Post,Put,Query} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
// import { CreateProfilDto } from '../dto/createProfil.dto';
import { ProfilService } from '../service/profil.service';
import { QueryOptions } from '../../../shared/utils/query-options';
@ApiTags('profils')
@Controller('profils')
export class ProfilController {
    constructor(private readonly ProfilService: ProfilService) {}
    @Get()
    public async getProfils(@Query() options: QueryOptions) {
        return {
            data: await this.ProfilService.getAll(options),
            message: `success`,
            status_code: HttpStatus.OK,
            meta:[],
        };
    }
    @ApiOperation({})
    @Get(':Id')
    public async getProfil(
        @Param('Id') Id: string,
    ) { 
        return {
            data: await this.ProfilService.getById(Id),
            message: `liste de vos profils`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
    @ApiOperation({})
    @Post('/user/:userId/entity/:entiteId')
    public async PostCreate(
        @Param('userId') userId: string,
        @Param('entiteId') entiteId: string,
        // @Body() createProfilDto: CreateProfilDto,
    ) {
        return {
            data: await this.ProfilService.create(
                userId,
                entiteId,
                // createProfilDto,
            ),
            message: `profil créer avec succes`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }

    @ApiOperation({})
    @Delete('delete')
    async delete(@Query('profilId') profilId: string) {
        return {
            data: await this.ProfilService.delete(profilId),
            message: `profil supprimé avec succes`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }

    @ApiOperation({})
    @Put(':Id')
    public async PostUpdate(
        @Param('Id') Id: string,
        /* @Body() UpdateProfilDto: CreateProfilDto, */
    ) {
        return {
            // data: await this.ProfilService.update(Id, UpdateProfilDto),
            message: `profil renommée avec succes`,
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
}
