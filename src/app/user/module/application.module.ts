import { HttpModule, Module } from '@nestjs/common';
import { ApplicationService } from '../service/application.service';
import { ProfilByApplicationService } from '../service/profilbyapplication.service';
import { ApplicationController } from '../controller/application.controller';
import { ProfilByApplicationController } from '../controller/ProfilByApplication.controller';
import { ProfilByApplicationProvider } from '../provider/ProfilByApplication.provider';
import { ApplicationProvider } from '../provider/Application.provider';
import { SharedModule } from '../../../shared/shared.module';
import { MailerModule } from '@nestjs-modules/mailer';
import {UserModule}from '../module/user.module'

@Module({
  imports: [SharedModule, HttpModule, MailerModule,UserModule],
  controllers: [ApplicationController, ProfilByApplicationController ],
  providers: [...ProfilByApplicationProvider, ...ApplicationProvider, ApplicationService, ProfilByApplicationService ],
  exports: [...ProfilByApplicationProvider, ...ApplicationProvider, ApplicationService, ProfilByApplicationService ],
})
export class ApplicationModule { }
