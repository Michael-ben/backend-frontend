import { Module } from '@nestjs/common';
import { FilesController } from '../controller/files.controller';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterConfigService } from '../service/multer-config.service';
import { FilesService } from '../service/files.service'

@Module({
    imports: [
        MulterModule.registerAsync({
            useClass: GridFsMulterConfigService,
        }),
    ],
    controllers: [FilesController],
    providers: [GridFsMulterConfigService, FilesService],
})
export class FilesModule {}