import { HttpModule, Module } from '@nestjs/common';
import { UserService } from '../service/user.service';
import { EntityService } from '../service/entity.service';
import { ProfilService } from '../service/profil.service';
import { UserController } from '../controller/user.controller';
import { EntityController } from '../controller/entity.controller';
import { ProfilController } from '../controller/profil.controller';
import { ProfilProvider } from '../provider/profil.provider';
import { UserProvider } from '../provider/user.provider';
import { EntityProvider } from '../provider/entity.provider';
import { SharedModule } from '../../../shared/shared.module';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
    imports: [SharedModule, HttpModule, MailerModule],
    controllers: [UserController, EntityController, ProfilController],
    providers: [
        ...UserProvider,
        ...ProfilProvider,
        ...EntityProvider,
        UserService,
        EntityService,
        ProfilService,
    ],
    exports: [
        ...UserProvider,
        ...ProfilProvider,
        UserService,
        EntityService,
        ProfilService,
    ],
})
export class UserModule {}
