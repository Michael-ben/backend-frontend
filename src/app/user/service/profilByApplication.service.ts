/* eslint-disable @typescript-eslint/camelcase */
import {Inject,Injectable,BadRequestException, NotFoundException} from '@nestjs/common';
import { CreateProfilByApplicationDto } from '../dto/ProfilByApplication.dto';
import { PaginateModel } from 'mongoose';
import { PROFILBYAPPLICATION_MODEL } from '../constants';
import { ProfilByApplication } from '../interface/profilByApplication.interface';
@Injectable()
export class  ProfilByApplicationService {
  constructor(
    @Inject( PROFILBYAPPLICATION_MODEL)
    private readonly profilByApplicationModel: PaginateModel<ProfilByApplication>,
  ) { }
  async getAll(options:
    | import('../../../../../../../../Users/ACER/projet/ged-api/src/shared/utils/query-options').QueryOptions
    | import('mongoose').PaginateOptions,
  ) {
    return this.profilByApplicationModel
      .paginate({}, { ...options, sort: { created_at: -1 } })
      .then(result => {
        const { docs: data, total, page, pages } = result;
        const meta = {
          current_page: page,
          first_page: 1,
          last_page: pages,
          total: total,
        };
        return { data, meta };
      });
  }
  async create(
    applicationId: string, profilId: string,
    createProfilByApplicationDto: CreateProfilByApplicationDto,
  ): Promise<ProfilByApplication> {

    const ProfilByApplication = new this.profilByApplicationModel({ ...createProfilByApplicationDto, application: applicationId, profil: profilId });
    return await ProfilByApplication.save();
  }
  async getById(Id: string): Promise<ProfilByApplication> {
    const profilByapplication = await this.profilByApplicationModel.findById(Id).catch(err => {
      throw new BadRequestException(err.message);
    });
    if (!profilByapplication) {
      throw new NotFoundException("Profil inexistant.");
    }
    return profilByapplication;
  }
  async delete(Id: string): Promise<ProfilByApplication> {
    const ProfilByApplication = await this. profilByApplicationModel.findByIdAndDelete(Id);
    return ProfilByApplication;

  }
  async update(
    Id: string,
    updateProfilByApplicationDto: CreateProfilByApplicationDto,
  ): Promise<ProfilByApplication> {
    const update = await this.profilByApplicationModel
      .findByIdAndUpdate(Id, updateProfilByApplicationDto, {
        new: true,
        runValidators: true,
      })
      .catch(err => {
        throw new BadRequestException(err.message);
      });
    if (!update) {
      throw new NotFoundException(" n'existe pas");
    }
    return update;
  }

}

