import { Inject, Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { User } from '../interface/user.interface';
import { USER_MODEL } from '../constants';
import * as bcrypt from 'bcrypt';
import { createUserDto } from '../dto/createUser.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { JwtService } from '@nestjs/jwt';
import {
    BadRequestException,
    NotFoundException,
    UnauthorizedException,
} from '../../../shared/exceptions';
import { PaginateModel } from 'mongoose';

const saltRounds = 10;

@Injectable()
export class UserService {
    constructor(
        @Inject(USER_MODEL)
        private readonly userModel: PaginateModel<User>,
    ) {}

    async getAll(
        options:
            | import('../../../shared/utils/query-options').QueryOptions
            | import('mongoose').PaginateOptions,
    ) {
        return this.userModel
            .paginate({}, { ...options, sort: { created_at: -1 } })
            .then(result => {
                const { docs: data, total, page, pages } = result;
                const meta = {
                    current_page: page,
                    first_page: 1,
                    last_page: pages,
                    total: total,
                };
                return { data, meta };
            });
    }

    async getById(userId: string): Promise<User> {
        const prospect = await this.userModel.findById(userId).catch(err => {
            throw new BadRequestException(err.message);
        });
        if (!prospect) {
            throw new NotFoundException("Cet utilisateur n'existe pas.");
        }
        return prospect;
    }

    async createUser(createUserDto: createUserDto): Promise<User> {
        const createUser = new this.userModel(createUserDto);
        const user = await createUser.save().catch(err => {
            throw new BadRequestException(err.message);
        });
        if (!createUser) {
            throw new NotFoundException("Cet utilisateur n'existe pas");
        }
        await this.checkPassword(createUser.password);
        createUser.salt = await bcrypt.genSalt();
        createUser.password = await bcrypt.hash(
        createUser.password,
            createUser.salt,
        );
        await this.userModel
            .findByIdAndUpdate(user._id, createUser, {
                new: true,
                runValidators: true,
            })
            .catch(err => {
                throw new BadRequestException(err.message);
            });
        
        return createUser;
    }
    async Update(userId: string, updateUserDto: createUserDto): Promise<User> {
        const updateUser = await this.userModel
            .findByIdAndUpdate(userId, updateUserDto, {
                new: true,
                runValidators: true,
            })
            .catch(err => {
                throw new BadRequestException(err.message);
            });
        if (!updateUser) {
            throw new NotFoundException("Cet utilisateur n'existe pas");
        }
        return updateUser;
    }

    /* async validateUserAccess(
        token: string,
        updateUserAccessDto: UpdateUserAccessDto,
    ) {
        const user = await this.userModel.findOne({
            confirmation_token: token,
        });
        if (!user) {
            throw new NotFoundException("Cet utilisateur n'existe pas");
        }
        await this.checkPassword(updateUserAccessDto.password);
        updateUserAccessDto.salt = await bcrypt.genSalt();
        updateUserAccessDto.password = await bcrypt.hash(
            updateUserAccessDto.password,
            updateUserAccessDto.salt,
        );
        await this.userModel
            .findByIdAndUpdate(user._id, updateUserAccessDto, {
                new: true,
                runValidators: true,
            })
            .catch(err => {
                throw new BadRequestException(err.message);
            });
        return user;
    } */

    async validateToken(token: string): Promise<boolean> {
        const isValidToken = await this.userModel.findOne({
            confirmation_token: token,
        });
        if (!isValidToken) {
            throw new UnauthorizedException('Token invalid');
        }
        return true;
    }
    async deleteUser(userId: string): Promise<User> {
        const deletedUser = await this.userModel.findByIdAndDelete(userId);
        return deletedUser;
    }
    public async validatePassword(
        password: string,
        salt: string,
        userId: string,
    ): Promise<boolean> {
        const user = await this.getById(userId);
        const hash = await bcrypt.hash(password, salt);
        return hash === user.password;
    }
    public async checkPassword(pass: string): Promise<boolean> {
        if (
            /^ (?=.* [A - Z])(?=.* [a - z])(?=.*\d)(?=.* [-+!* $@% _]) ([-+!* $@% _\w]{ 8, 15 }) $/.test(
                pass,
            )
        ) {
            throw new BadRequestException('Mot de passe faible');
        }
        return true;
    }
    //Un mot de passe valide aura
    //  - de 8 à 15 caractères
    //   - au moins une lettre minuscule
    // - au moins une lettre majuscule
    // - au moins un chiffre
    //  - au moins un de ces caractères spéciaux: $ @ % * + - _!
    //  - aucun autre caractère possible: pas de & ni de { par exemple)

    async findOneByUsername(username): Promise<User> {
        return await this.userModel.findOne({ username: username });
    }
    async findOneByEmail(email): Promise<User> {
        return await this.userModel.findOne({ email: email });
    }
    async setPassword(email: string, newPassword: string): Promise<boolean> {
        const userFromDb = await this.userModel.findOne({ email: email });
        if (!userFromDb)
            throw new HttpException(
                'LOGIN.USER_NOT_FOUND',
                HttpStatus.NOT_FOUND,
            );

        userFromDb.password = await bcrypt.hash(newPassword, saltRounds);

        await userFromDb.save();
        return true;
    }
}
