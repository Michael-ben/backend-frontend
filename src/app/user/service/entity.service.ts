/* eslint-disable @typescript-eslint/no-unused-vars */
import { Inject, Injectable } from '@nestjs/common';
import { Entity } from '../interface/entity.interface';
import { ENTITY_MODEL } from '../constants';
import { registerEntityDto } from '../dto/registerEntity.dto';
import {BadRequestException,NotFoundException} from '../../../shared/exceptions';
import { PaginateModel } from 'mongoose';

@Injectable()
export class EntityService {
    constructor(
        @Inject(ENTITY_MODEL)
        private readonly entityModel: PaginateModel<Entity>,
    ) {}

    async getAll(
        options:
            | import('../../../../../../../../Users/ACER/projet/ged-api/src/shared/utils/query-options').QueryOptions
            | import('mongoose').PaginateOptions,
    ) {
        return this.entityModel
            .paginate({}, { ...options, sort: { created_at: -1 } })
            .then(result => {
                const { docs: data, total, page, pages } = result;
                const meta = {
                    current_page: page,
                    first_page: 1,
                    last_page: pages,
                    total: total,
                };
                return { data, meta };
            });
    }

    async getById(Id: string): Promise<Entity> {
        const prospect = await this.entityModel.findById(Id).catch(err => {
            throw new BadRequestException(err.message);
        });
        if (!prospect) {
            throw new NotFoundException("Cette entité n'existe pas.");
        }
        return prospect;
    }
    async updateEntity(
        Id: string,
        updateEntityDto: registerEntityDto,
    ): Promise<Entity> {
        const update = await this.entityModel
            .findByIdAndUpdate(Id, updateEntityDto, {
                new: true,
                runValidators: true,
            })
            .catch(err => {
                throw new BadRequestException(err.message);
            });
        if (!update) {
            throw new NotFoundException("Cette entité n'existe pas");
        }
        return update;
    }
    async createEntity(
        createEntiteDTO: registerEntityDto,
    ): Promise<Entity> {
        const entity = new this.entityModel({ ...createEntiteDTO });
        return await entity.save();
    }
}
