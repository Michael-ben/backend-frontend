/* eslint-disable @typescript-eslint/no-unused-vars */
import { Injectable, HttpException, HttpStatus , Inject, BadRequestException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose'
import { Connection } from 'mongoose'
import { MongoGridFS } from 'mongo-gridfs'
import { GridFSBucketReadStream } from 'mongodb'
import { FileInfoVm } from '../interface/file-info-vm.model'
import {GetFilesFilterDto} from '../dto/GetFile.dto'
import {UpdateFileDto}from '../dto/updateFile.dto'
import { promises } from 'fs';
import { ApiResponseProperty } from '@nestjs/swagger';
import { filter } from 'compression';
import { Stream } from 'stream';



@Injectable()
export class FilesService {
  private fileModel: MongoGridFS;

  constructor(@InjectConnection() private readonly connection: Connection) {
    this.fileModel = new MongoGridFS(this.connection.db, 'fs');
  }
   async getAll(filter:any): Promise<FileInfoVm[]>{
   return await this.fileModel.find(filter).catch( err => {throw new HttpException('File not found', HttpStatus.NOT_FOUND)} )
   .then(result => result)

} 
 async getFileByFilename(filterDto:GetFilesFilterDto):Promise<FileInfoVm[]>{
  const{search} = filterDto;
  let files = await  this.getAll(search)
  if(search){
    files = files.filter(files =>
      files.filename.includes(search));
  }
  return files
 }
  async readStream(id: string): Promise<GridFSBucketReadStream> {
    return await this.fileModel.readFileStream(id);
  }

  async findInfo(id: string): Promise<FileInfoVm> {
    const result = await this.fileModel
      .findById(id).catch( err => {throw new HttpException('File not found', HttpStatus.NOT_FOUND)} )
      .then(result => result)
    return{
      filename: result.filename,
      length: result.length,
      chunkSize: result.chunkSize,
      md5: result.md5,
      uploadDate:result.uploadDate,
      contentType: result.contentType      
    }
  }
   async deleteFile(id: string ):Promise<boolean>{
    return await this.fileModel.delete(id)
  }
  async update(id:Stream ,UpdateFileDto:UpdateFileDto ):Promise<FileInfoVm>{
    return await this.fileModel.writeFileStream(id,  UpdateFileDto)
  .catch(err => {
      throw new BadRequestException(err.message);
  });
  }
}