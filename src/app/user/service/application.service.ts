/* eslint-disable @typescript-eslint/camelcase */
import {
  Inject,
  Injectable,
  BadRequestException, NotFoundException
} from '@nestjs/common';
import { CreateApplicationDto  } from '../dto/Application.dto';
import { Application } from '../interface/application.interface';
import { PaginateModel } from 'mongoose';
import { APPLICATION_MODEL } from '../constants';
@Injectable()
export class ApplicationService {
  constructor(

    @Inject(APPLICATION_MODEL)
    private readonly applicationModel: PaginateModel<Application>,
  ) {}
  async getAll(options:
    | import('../../../../../../../../Users/ACER/projet/ged-api/src/shared/utils/query-options').QueryOptions
    | import('mongoose').PaginateOptions,
  ) {
    return this.applicationModel
      .paginate({}, { ...options, sort: { created_at: -1 } })
      .then(result => {
        const { docs: data, total, page, pages } = result;
        const meta = {
          current_page: page,
          first_page: 1,
          last_page: pages,
          total: total,
        };
        return { data, meta };
      });
    
  }
  async createApplication(
    entiteId: string,
    CreateApplicationDto: CreateApplicationDto ,
  ): Promise<Application> {

    const application = new this.applicationModel({ ...CreateApplicationDto ,entity: entiteId });
    return await application.save();
  }
  async getById(Id: string): Promise<Application> {
    const application = await this.applicationModel.findById(Id).catch(err => {
      throw new BadRequestException(err.message);
    });
    if (!application) {
      throw new NotFoundException("Cette application n'existe pas.");
    }
    return application;
  }
  async delete(Id: string): Promise<Application> {
    const deleteApplication = await this.applicationModel.findByIdAndDelete(Id);
    return deleteApplication;
  }
  async updateApplication(
    Id: string,
    updateApplicationDto: CreateApplicationDto ,
  ): Promise<Application> {
    const update = await this.applicationModel
      .findByIdAndUpdate(Id, updateApplicationDto, {
        new: true,
        runValidators: true,
      })
      .catch(err => {
        throw new BadRequestException(err.message);
      });
    if (!update) {
      throw new NotFoundException("Cette application  n'existe pas");
    }
    return update;
  }

}

