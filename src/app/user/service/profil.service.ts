/* eslint-disable @typescript-eslint/camelcase */
import {
    Inject,
    Injectable,
    BadRequestException,
    NotFoundException,
} from '@nestjs/common';
// import { CreateProfilDto } from '../dto/createProfil.dto';
import { Profil } from '../interface/profil.interface';
import { PaginateModel } from 'mongoose';
import { PROFIL_MODEL } from '../constants';

@Injectable()
export class ProfilService {
    constructor(
        @Inject(PROFIL_MODEL)
        private readonly profilModel: PaginateModel<Profil>,
    ) {}
    async getAll(
        options:
            | import('../../../../../../../../Users/ACER/projet/ged-api/src/shared/utils/query-options').QueryOptions
            | import('mongoose').PaginateOptions,
    ) {
        return this.profilModel
            .paginate({}, { ...options, sort: { created_at: -1 } })
            .then(result => {
                const { docs: data, total, page, pages } = result;
                const meta = {
                    current_page: page,
                    first_page: 1,
                    last_page: pages,
                    total: total,
                };
                return { data, meta };
            });
    }
    async create(
        userId: string,
        entiteId: string,
        // createProfilDto: CreateProfilDto,
    ): Promise<Profil> {
        const profil = new this.profilModel({
            // ...createProfilDto,
            user: userId,
            entity: entiteId,
        });
        return await profil.save();
    }
    async getById(Id: string): Promise<Profil> {
        const profil = await this.profilModel.findById(Id).catch(err => {
            throw new BadRequestException(err.message);
        });
        if (!profil) {
            throw new NotFoundException("Cette entité n'existe pas.");
        }
        return profil;
    }
    async delete(Id: string): Promise<Profil> {
        const deletedProfil = await this.profilModel.findByIdAndDelete(Id);
        return deletedProfil;
    }

 /* async update(
        Id: string,
         CreateProfilDto : CreateProfilDto,
    ): Promise<Profil> {
        const updateProfil = await this.profilModel
            .findByIdAndUpdate(Id, CreateProfilDto , {
                new: true,
                runValidators: true,
            })
            .catch(err => {
                throw new BadRequestException(err.message);
            });
        if (!updateProfil) {
            throw new NotFoundException("Ce profil n'existe pas");
        }
        return updateProfil;
    }  */
}
