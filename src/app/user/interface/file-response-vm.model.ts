import { ApiHideProperty } from '@nestjs/swagger';
import { FileInfoVm } from './file-info-vm.model';

export class FileResponseVm {
    @ApiHideProperty() 
    message: string;
    @ApiHideProperty()
    file: FileInfoVm;
}