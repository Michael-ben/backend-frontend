import { ApiHideProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';



export class FileInfoVm {

    @ApiHideProperty()
    @Expose()
    length: number;

    @ApiHideProperty()
    @Expose()
    chunkSize: number;

    @ApiHideProperty()
    @Expose()
    filename: string;    

    @ApiHideProperty()
    @Expose()
    md5: string;

    @ApiHideProperty()
    @Expose()
    contentType: string;
    @ApiHideProperty()
    @Expose()
    uploadDate: Date;
}