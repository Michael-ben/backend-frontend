import { Document } from 'mongoose';

export interface Application extends Document {
  // Identification
  readonly app_name: string;
  readonly app_code: string;
  readonly created_by: string;
  readonly updated_by: string;
  readonly created_at: Date;
  readonly updated_at: Date;
}
