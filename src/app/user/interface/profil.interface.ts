import { Document } from 'mongoose';

export interface Profil extends Document {
  // Identification
  readonly entity: string;
  readonly user: string;
  readonly created_by: string;
  readonly updated_by: string;
  readonly created_at: Date;
  readonly updated_at: Date;
}