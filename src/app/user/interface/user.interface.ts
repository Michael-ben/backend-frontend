import { Document } from 'mongoose';

export interface User extends Document {
    readonly first_name: string;
    readonly last_name: string;
    readonly email: string;
    readonly username: string;
    password: string;
    salt: string;
    readonly created_by: string;
    readonly updated_by: string;
    readonly created_at: Date;
    readonly updated_at: Date;
}
