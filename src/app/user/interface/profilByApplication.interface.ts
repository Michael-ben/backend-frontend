import { Document } from 'mongoose';

export interface ProfilByApplication extends Document {
  readonly profil: string
  readonly application: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}