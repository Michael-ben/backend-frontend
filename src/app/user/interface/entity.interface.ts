import { Document } from 'mongoose';

export interface Entity extends Document {
    // Identification
    readonly first_name: string;
    readonly last_name: string;
    readonly company_name: string;
    readonly email: string;
    readonly created_by: string;
    readonly updated_by: string;
    readonly created_at: Date;
    readonly updated_at: Date;
}
