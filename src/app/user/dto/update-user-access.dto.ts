import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class UpdateUserAccessDto {
           @IsString()
           @IsNotEmpty()
           @ApiProperty()
           @ApiProperty({ type: String, description: 'username' })
           readonly username: string;

           @IsString()
           @IsNotEmpty()
           @MinLength(8)
           @ApiProperty({ type: String, description: 'password' })
           password: string;

           salt: string;
       }
