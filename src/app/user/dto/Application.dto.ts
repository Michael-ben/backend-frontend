/* eslint-disable @typescript-eslint/class-name-casing */
import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsString,
} from 'class-validator';
export class CreateApplicationDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly app_name: string;
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly app_code: string;
 
}
