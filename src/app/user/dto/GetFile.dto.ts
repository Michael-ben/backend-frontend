import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsNotEmpty,IsString } from 'class-validator';
export class GetFilesFilterDto  {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiProperty({ required: false })
    readonly search: string;
}

