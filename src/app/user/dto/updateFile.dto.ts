import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsNotEmpty,IsString } from 'class-validator';
export class UpdateFileDto  {
    @IsString()
    @IsNotEmpty()
    @IsOptional()
    @ApiProperty({ required: false })
    readonly filename: string;
}

