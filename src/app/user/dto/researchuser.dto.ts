import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
export class CreateResearchUserDto{
    @IsString()
    @IsOptional()
    @ApiProperty({ required: false })
    readonly username: string;
}
