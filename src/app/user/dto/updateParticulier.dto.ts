/* eslint-disable @typescript-eslint/class-name-casing */
import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEmail, IsOptional, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class updateParticulierDto {
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'first_name' })
           readonly first_name: string;
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'last_name' })
           readonly last_name: string;
           @Type(() => Date)
           @IsDate()
           @IsOptional()
           @ApiProperty({ required: false })
           @IsEmail()
           @ApiProperty({ required: true })
           readonly email: string;
       }
