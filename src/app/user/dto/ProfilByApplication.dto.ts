/* eslint-disable @typescript-eslint/class-name-casing */
import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsString,
} from 'class-validator';
export class CreateProfilByApplicationDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly can_send_sms: boolean;
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly can_send_email: boolean;
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly can_send_web_push : boolean;
  
}
