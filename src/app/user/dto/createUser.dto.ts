/* eslint-disable @typescript-eslint/class-name-casing */
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsString, IsNotEmpty, MinLength } from 'class-validator';
export class createUserDto {
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'first_name' })
           readonly first_name: string;
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'last_name' })
           readonly last_name: string;
           @IsString()
           @IsNotEmpty()
           @ApiProperty()
           @ApiProperty({ type: String, description: 'username' })
           readonly username: string;

           @IsString()
           @IsNotEmpty()
           @MinLength(8)
           @ApiProperty({ type: String, description: 'password' })
           password: string;
           salt: string;
           @IsEmail()
           @ApiProperty({ type: String, description: 'email' })
           readonly email: string;
       }
