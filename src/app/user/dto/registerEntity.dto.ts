/* eslint-disable @typescript-eslint/class-name-casing */
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsString } from 'class-validator';
export class registerEntityDto {
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'first_name' })
           readonly first_name: string;
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'last_name' })
           readonly last_name: string;
           @IsString()
           @IsOptional()
           @ApiProperty({ type: String, description: 'company_name' })
           readonly company_name: string;
           @IsEmail()
           @ApiProperty({ required: true })
           readonly email: string;
       }
