export const USER_MODEL = 'USER_MODEL';
export const ENTITY_MODEL = 'ENTITY_MODEL';
export const PROFIL_MODEL = 'Profil';
export const APPLICATION_MODEL = 'APPLICATION_MODEL';
export const PROFILBYAPPLICATION_MODEL = 'PROFILBYAPPLICATION_MODEL'
export const USER_COLLECTION = 'User';
export const PROFIL_COLLECTION = 'Profil';
export const ENTITY_COLLECTION = 'Entity';
export const PROFILBYAPPLICATION_COLLECTION = 'ProfilByApplication';
export const APPLICATION_COLLECTION = 'APPLICATION'
export const jwtConstants = {
    secret: 'secretged',
    expiresIn: '3600',
};
