import { DB_PROVIDER } from '../../../shared/constants';
import { Connection } from 'mongoose';
import { PROFIL_COLLECTION, PROFIL_MODEL } from '../constants';
import { ProfilSchema } from '../schema/profil.schema';
import { Profil } from '../interface/profil.interface';

export const ProfilProvider = [
    {
        provide: PROFIL_MODEL,
        useFactory: (connection: Connection) => {
            ProfilSchema.pre<Profil>('save', async function(next) {
                next();
            });
            return connection.model(PROFIL_COLLECTION, ProfilSchema);
        },
        inject: [DB_PROVIDER],
    },
];
