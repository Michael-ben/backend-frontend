import { DB_PROVIDER } from '../../../shared/constants';
import { Connection } from 'mongoose';
import { APPLICATION_COLLECTION, APPLICATION_MODEL } from '../constants';
import {ApplicationSchema } from '../schema/application.schema';
import {Application} from '../interface/application.interface';

export const ApplicationProvider = [
  {
    provide: APPLICATION_MODEL,
    useFactory: (connection: Connection) => {
      ApplicationSchema.pre<Application>('save', async function (next) {
        next();
      });
      return connection.model(APPLICATION_COLLECTION, ApplicationSchema);
    },
    inject: [DB_PROVIDER],
  },
];