import { DB_PROVIDER } from '../../../shared/constants';
import { Connection } from 'mongoose';
import { USER_COLLECTION, USER_MODEL } from '../constants';
import { UserSchema } from '../schema/user.schema';
import { generateUniqueCode } from '../../../shared/utils/generate-unique-code';
import { User } from '../interface/user.interface';

export const UserProvider = [
    {
        provide: USER_MODEL,
        useFactory: (connection: Connection) => {
            UserSchema.pre<User>('save', async function(next) {
                next();
            });
            return connection.model(USER_COLLECTION, UserSchema);
        },
        inject: [DB_PROVIDER],
    },
];
