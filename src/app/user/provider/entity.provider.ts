import { DB_PROVIDER } from '../../../shared/constants';
import { Connection } from 'mongoose';
import { ENTITY_COLLECTION, ENTITY_MODEL } from '../constants';
import { EntitySchema } from '../schema/entity.schema';
import { Entity } from '../interface/entity.interface';

export const EntityProvider = [
    {
        provide: ENTITY_MODEL,
        useFactory: (connection: Connection) => {
            EntitySchema.pre<Entity>('save', async function(next) {
                next();
            });
            return connection.model(ENTITY_COLLECTION, EntitySchema);
        },
        inject: [DB_PROVIDER],
    },
];
