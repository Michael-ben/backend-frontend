import { DB_PROVIDER } from '../../../shared/constants';
import { Connection } from 'mongoose';
import { PROFILBYAPPLICATION_COLLECTION, PROFILBYAPPLICATION_MODEL } from '../constants';
import { ProfilByApplicationSchema } from '../schema/profileByApplication.schema';
import { ProfilByApplication } from '../interface/profilByApplication.interface';

export const ProfilByApplicationProvider = [
  {
    provide: PROFILBYAPPLICATION_MODEL,
    useFactory: (connection: Connection) => {
      ProfilByApplicationSchema.pre<ProfilByApplication>('save', async function (next) {
        next();
      });
      return connection.model(PROFILBYAPPLICATION_COLLECTION, ProfilByApplicationSchema);
    },
    inject: [DB_PROVIDER],
  },
];
