import { Module } from '@nestjs/common';
import { SharedModule } from '../shared/shared.module';
import {AuthModule}from '../app/auth/auth.module';
import { UserModule } from './user/module/user.module';
import {ApplicationModule}from './user/module/application.module';
import { FilesModule } from './user/module/files.module';
import { MulterModule } from '@nestjs/platform-express';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
    imports: [
        UserModule,
        AuthModule,
        SharedModule,
        ApplicationModule,
        MulterModule.register({ dest: './uploads' }),
        MongooseModule.forRoot('mongodb://localhost/db_dtx_0', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false,
        }),
        FilesModule,
    ],
})
export class AppModule {}
