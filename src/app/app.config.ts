import {Injectable} from '@nestjs/common';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cors from 'cors';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as express from 'express';
import * as helmet from 'helmet';
import * as nocache from 'nocache';


@Injectable()
export class AppConfiguration {
    public configure(express: express.Application) {
        express
            .options('*', cors())
            .use(cors())
            .use(helmet())
            .use(nocache())
            .use(
                helmet.hsts({
                    maxAge: 15768000,
                    includeSubDomains: true,
                }),
            )
            .use(compression())
            .use(bodyParser.json())
            .use(
                bodyParser.urlencoded({
                    extended: true,
                }),
            );
        return express;
    }
}
