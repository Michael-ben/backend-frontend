import * as express from 'express';

export class AppBootstrap {
    private isProd: any = false;

    public expressAppDefinition(app: express.Application): express.Application {
        app.set('prefix', process.env.APP_URL_PREFIX);
        app.set('host', process.env.APP_HOST);
        app.set('port', this.normalizedPort(process.env.APP_PORT || '3000'));

        return app;
    }

    public normalizedPort(port: string): number | string {
        const portAsNumber = parseInt(port, 10);
        if (isNaN(portAsNumber)) {
            return port;
        }
        if (portAsNumber >= 0) {
            return portAsNumber;
        }
        return;
    }
}
