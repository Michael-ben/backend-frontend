/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from '../service/auth.service';
import { UserModule } from '../../user/module/user.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../constants';
import { userProvider } from '../../user/provider/user.provider';
import { UserService } from '../../user/service/user.service';
import { UserController } from '../../user/controller/user.controller';
import { SharedModule } from '../../../shared/shared.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { toMatchOneOf, toMatchShapeOf } from 'jest-to-match-shape-of';
import { USER_MODEL } from '../../user/constants';
expect.extend({
    toMatchOneOf,
    toMatchShapeOf,
});

describe('Auth Controller', () => {
    let controller: AuthController;
    let modelUser;
    let data: {
        username: 'Fousseni';
        password: '12345678';
        structure_id: '5e873bb675aa970a90c60680';
    };
    let response;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthController, UserController],
            providers: [AuthService, ...userProvider, UserService],
            imports: [
                UserModule,
                PassportModule,
                JwtModule.register({
                    secret: jwtConstants.secret,
                    signOptions: { expiresIn: jwtConstants.expiresIn },
                }),
                SharedModule,
                MailerModule.forRoot({
                    transport: {
                        host: 'localhost',
                        port: 1025,
                        ignoreTLS: true,
                        secure: false,
                        auth: {
                            user: process.env.MAILDEV_INCOMING_USER,
                            pass: process.env.MAILDEV_INCOMING_PASS,
                        },
                    },
                    defaults: {
                        from: '"No Reply" <no-reply@localhost>',
                    },
                    preview: true,
                    template: {
                        dir: process.cwd() + '/templates/',
                        options: {
                            strict: true,
                        },
                    },
                }),
            ],
        }).compile();

        controller = module.get<AuthController>(AuthController);
        modelUser = module.get(USER_MODEL);
    });

    it('SignIn user', async () => {});

    describe('postLogin', () => {
        it('It should authentificate a new  user', async () => {
            const user = await new modelUser(data);
            const expectedResponse = {
                data: user,
                message: 'created',
                status_code: HttpStatus.CREATED,
                meta: [],
            };
            jest.spyOn(controller, 'postLogin').mockResolvedValue(
                expectedResponse,
            );
            const response = await controller.postLogin(data);
            expect(response).toMatchShapeOf(expectedResponse);
        }, 99999);
    });
});
