/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable no-var */
import { Body, Controller, HttpStatus, Post } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { ApiTags } from '@nestjs/swagger';
import { AuthLoginDto } from '../dto/auth-login.dto';

@ApiTags('Auth')
@Controller()
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('/login')
    async postLogin(@Body() authLoginDto: AuthLoginDto) {
        return {
            data: await this.authService.login(authLoginDto),
            message: 'authenticated',
            status_code: HttpStatus.OK,
            meta: [],
        };
    }
}
