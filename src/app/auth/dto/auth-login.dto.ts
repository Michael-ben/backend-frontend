import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class AuthLoginDto {
    @IsString()
    @ApiProperty()
    username: string;
    @ApiProperty()
    @IsString()
    password: string;
}
