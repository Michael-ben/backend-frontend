import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';
import { UnauthorizedException } from '../../../shared/exceptions';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private jwtService: JwtService) {}
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const accessToken = context.switchToHttp().getRequest();

        if (!this.jwtService.verify(accessToken)) {
            throw new UnauthorizedException('Session expiré');
        }
        return true;
    }
}