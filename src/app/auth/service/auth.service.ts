import { UserService } from '../../user/service/user.service';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { NotFoundException } from '../../../shared/exceptions';
import { AuthLoginDto } from '../dto/auth-login.dto';
@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService,
    ) {}

    async login(loginDTO: AuthLoginDto) {
        const { username, password } = loginDTO;
        const user = await this.userService.findOneByUsername(username);
        if (!user) {
            throw new NotFoundException(
                'Le nom utilisateur ou le mot de passe est incorrect',
            );
        }
        const { salt, _id } = user;
        const pass = await this.userService.validatePassword(
            password,
            salt,
            _id,
        );
        if (pass === false) {
            throw new NotFoundException('le mot de passe est incorrect');
        }
        const payload = { username: username, sub: _id };
        const accessToken = this.jwtService.sign(payload);
        return {
            access_token: accessToken,
            expire_at: Date.now() + 1000 * 60 * 60,
            refresh_token: '',
        };
    }
}
