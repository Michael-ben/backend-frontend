import { Module } from '@nestjs/common';
import { AuthService } from './service/auth.service';
import { AuthController } from './controller/auth.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { UserModule } from '../user/module/user.module';
import { LocalStrategy } from './stretegy/local.strategy';
@Module({
    imports: [
        UserModule,
        /* forgotPasswordModule, */
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
    ],
    providers: [AuthService, LocalStrategy],
    controllers: [AuthController],
    exports: [LocalStrategy],
})
export class AuthModule {}
