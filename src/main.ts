import { NestFactory } from '@nestjs/core';
import {
    ExpressAdapter,
    NestExpressApplication,
} from '@nestjs/platform-express';
import { AppModule } from './app/app.module';
import { AppComponent } from './app/app.component';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { version } from '../package.json';
import { ValidationPipe } from '@nestjs/common';

const appInstance = new AppComponent();
const app = appInstance.bootstrap();

async function bootstrap() {
    const server = await NestFactory.create<NestExpressApplication>(
        AppModule,
        new ExpressAdapter(app),
    );
    server.setGlobalPrefix('v1').useGlobalPipes(
        new ValidationPipe({
            whitelist: true,
            transform: true,
        }),
    );

    const options = new DocumentBuilder()
        .setTitle('ged-api')
        .setDescription('This describes all ged API endpoints')
        .setVersion(version)
        .build();
    const document = SwaggerModule.createDocument(server, options);
    SwaggerModule.setup('docs', server, document);

    await server.init();
    await server.listen(app.get('port'));
    SwaggerModule.setup('api', server, document);
}

bootstrap();
