import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from '../../../src/app/auth/auth.module';
import { SharedModule } from '../../../src/shared/shared.module';

describe('AuthController (e2e)', () => {
    let app: INestApplication;
    let data: {
        username: 'Fousseni';
        password: '12345678';
        structure_id: '5e873bb675aa970a90c60680';
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AuthModule, SharedModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/v1/auths  (POST)', async () => {
        const response = await request(await app.getHttpServer())
            .post('/auths')
            .send(data)
            .set('Accept', 'application/json');
        expect(({ body }) => {
            expect(body);
        });
        expect(response.status).toBe(HttpStatus.NOT_FOUND);
    });
});
