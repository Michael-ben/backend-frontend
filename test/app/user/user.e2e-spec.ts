import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { UserModule } from '../../../src/app/user/module/user.module';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';
import { SharedModule } from '../../../src/shared/shared.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../../src/app/auth/constants';

describe('UserController (e2e)', () => {
    let app: INestApplication;

    const data = {
        email: 'dev_e2e@akilcab.com',
        last_name: 'null',
        first_name: 'null',
        structure_id: 'null',
        type_structure: 'null',
    };

    let token = '';

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [
                UserModule,
                SharedModule,
                JwtModule.register({
                    secret: jwtConstants.secret,
                    signOptions: { expiresIn: jwtConstants.expiresIn },
                }),
            ],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const privatePem = fs.readFileSync('./private.pem', 'utf8');
        token = jwt.sign(
            { username: 'test' },
            { key: privatePem, passphrase: 'jenkinsPrivatePem' },
            { algorithm: 'RS256' },
        );
    });

    /*it('/v1/users (POST)', async () => {
        const response = await request(await app.getHttpServer())
            .post('/users')
            .send(data)
            .set('Authorization', `Bearer ${token}`)
            .set('Accept', 'application/json');
        expect(({body}) => {
            expect(body);
        });
        expect(response.status).toBe(HttpStatus.CREATED);
    });*/

    it('/v1/users?offset=0&limit=3  (GET)', async () => {
        const response = await request(await app.getHttpServer())
            .get(`/users`)
            .set('Authorization', `Bearer ${token}`);
        expect(response.status).toBe(HttpStatus.OK);
    });

    it('/v1/users/:id  (GET)', async () => {
        const id = '5e873bb675aa970a40c61680';
        const response = await request(await app.getHttpServer())
            .get(`/users/${id}`)
            .set('Authorization', `Bearer ${token}`);
        expect(response.status).toBe(HttpStatus.NOT_FOUND);
    });

    it('/v1/users/:id  (DELETE)', async () => {
        const id = '5e790dc029817f040b07130d';
        const response = await request(await app.getHttpServer())
            .delete(`/users/${id}`)
            .set('Authorization', `Bearer ${token}`);
        expect(response.status).toBe(HttpStatus.NOT_FOUND);
    });

    it('/v1/users/:id  (UPDATE)', async () => {
        const id = '5e768dc020817f040b07130d';
        const updateData = {
            username: 'Fousseni',
            email: 'foussenni@febe.ci',
            avatar_url: 'null_null',
            civility: 'null',
            last_name: 'null',
            first_name: 'null',
            remember: false,
            structure_id: 'null',
            type_structure: 'null',
            role: 'null',
        };
        const response = await request(await app.getHttpServer())
            .put(`/users/${id}`)
            .send(updateData)
            .set('Authorization', `Bearer ${token}`)
            .set('Accept', 'application/json');
        expect(({ body }) => {
            expect(body);
        });
        expect(response.status).toBe(HttpStatus.NOT_FOUND);
    });

    /* it('/v1/users/confirmation-tokens/:token  (UPDATE)', async () => {
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IkZvdXNzZW5pQGdtYWlsLmNvbSIsInN0cnVjdHVyZV9pZCI6Im51bGwiLCJpYXQiOjE1ODY2NjIxOTcsImV4cCI6MTU4NjY2MjIwMH0.amCSVxWutSA1He0dYxMaMOw4yM8oTsitZkj5pfPKwJU';
        const updateDataAccess = {
            username: 'Fousseni',
            password: 'P@$$w0rd',
            salt: '',
        };
        const response = await request(await app.getHttpServer())
            .put(`/users/confirmation-tokens/${token}`)
            .send(updateDataAccess)
            .set('Accept', 'application/json');
        expect(({ body }) => {
            expect(body);
        });
        expect(response.status).toBe(HttpStatus.FORBIDDEN);
    });

    it('/v1/users/confirmation-tokens/:token  (UPDATE)', async () => {
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IkZvdXNzZW5pQGdtYWlsLmNvbSIsInN0cnVjdHVyZV9pZCI6Im51bGwiLCJpYXQiOjE1ODY2NjIxOTcsImV4cCI6MTU4NjY2MjIwMH0.amCSVxWutSA1He0dYxMaMOw4yM8oTsitZkj5pfPKwJU';
        const updateDataAccess = {
            username: 'Fousseni',
            password: 'P@$$w0rd',
            salt: '',
        };
        const response = await request(await app.getHttpServer())
            .put(`/users/confirmation-tokens/${token}`)
            .send(updateDataAccess)
            .set('Accept', 'application/json');
        expect(({ body }) => {
            expect(body);
        });
        expect(response.status).toBe(HttpStatus.FORBIDDEN);
    });

    it('/v1/users/confirmation-tokens/:token  (GET)', async () => {
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6IkZvdXNzZW5pQGdtYWlsLmNvbSIsInN0cnVjdHVyZV9pZCI6Im51bGwiLCJpYXQiOjE1ODY2NjIxOTcsImV4cCI6MTU4NjY2MjIwMH0.amCSVxWutSA1He0dYxMaMOw4yM8oTsitZkj5pfPKwJU';
        const response = await request(await app.getHttpServer())
            .get(`/users/confirmation-tokens/${token}`);
        expect(response.status).toBe(HttpStatus.FORBIDDEN);
    });*/
});
