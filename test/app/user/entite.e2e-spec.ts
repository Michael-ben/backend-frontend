import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { UserModule } from '../../../src/app/user/module/user.module';
import { SharedModule } from '../../../src/shared/shared.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../../../src/app/auth/constants';

describe('EntiteController (e2e)', () => {
  let app: INestApplication;


  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        UserModule,
        SharedModule,
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: jwtConstants.expiresIn },
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

  });

   it('/v1/entities?offset=0&limit=3  (GET)', async () => {
    const response = await request(await app.getHttpServer())
      .get(`/entities`)
    expect(response.status).toBe(HttpStatus.OK);
  }); 

  it('/v1/entities/:id  (GET)', async () => {
    const id = '5e873bb675aa970a40c61680';
    const response = await request(await app.getHttpServer())
      .get(`/user/${id}`)
    expect(response.status).toBe(HttpStatus.NOT_FOUND);
  });

  it('/v1/entites/:id  (DELETE)', async () => {
    const id = '5e790dc029817f040b07130d';
    const response = await request(await app.getHttpServer())
      .delete(`/entites/${id}`)
    expect(response.status).toBe(HttpStatus.NOT_FOUND);
  });

  it('/v1/entites/:id  (UPDATE)', async () => {
    const id = '5e768dc020817f040b07130d';
    const updateData = {
      first_name: 'Fousseni',
      last_name : 'olsby',
      email: 'bkoua343@gmail.com'
    };
    const response = await request(await app.getHttpServer())
      .put(`/entities/${id}`)
      .send(updateData)
      .set('Accept', 'application/json');
    expect(({ body }) => {
      expect(body);
    });
    expect(response.status).toBe(HttpStatus.NOT_FOUND);
  });
});
