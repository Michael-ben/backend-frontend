# Notify Application programing interface (API)
DigitExpert API is all need to handle API for DigitExpert UI, it’s build in Nodejs with [Nest](https://github.com/nestjs/nest) and documentation is [available](http://api-st.digitexpert.com/docs).<br/>
This following schema describes the implemented modules : <br/>
![<img src="descriptionv1.0.0.png" width="250"/>](descriptionv1.0.0.png)
## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:

* Git - [Download & Install Git](https://git-scm.com/downloads). OSX and Linux machines typically have this already installed.
* Node.js - [Download & Install Node.js](https://nodejs.org/en/download/) and the npm package manager. If you encounter any problems, you can also use this GitHub Gist to install Node.js.
* MongoDB - [Download & Install MongoDB](https://www.mongodb.com/download-center), and make sure it's running on the default port (27017).

## Installation

### Cloning the GitLab Repository
```bash
$ git clone https://gitlab.com/akiltech/digit-main-api.git
```
### Quick install
To install the dependencies, run this in the application folder from the command-line:
```bash
$ npm install
```

## Running your application
Run your application using npm:
```bash
$ npm start
```
Your application should run on port 3000 with the development environment configuration, so in your browser just go to http://localhost:3000

That's it! Your application should be running. To proceed with your development, check the other sections in this documentation. If you encounter any problems, try the Troubleshooting section.

Explore config/env/development.js for development environment configuration options.

### Running in production mode
To run your application with production environment configuration:
```
$ npm run start:prod
```
Explore config/env/production.js for production environment configuration options.

### Running in development mode
To run your application with production development configuration:
```
$ npm run start
```
Explore config/env/development.js for development environment configuration options.

## Testing your application
You can run the full test suite included with the test task:
```
$ npm run test
```
To execute only e2e tests, run the test:e2e task:
```
$ npm run test:e2e
```
And to run only coverage tests, run the test:client task:
```
$ npm run test:cov
```

## Development and deployment With Docker
* Install Docker
* Install Compose
* Local development and testing with compose:
``` 
$ docker-compose up
```

## Production deploy with Docker
* Production deployment with compose:
``` 
$ docker-compose -f docker-compose.yml up -d
```

## Versionning
We use [Superman Versionning](https://github.com/TheMartianGeeks/superman) and at each version we create a new branch.

## Author
[AkilTechnologies](https://akiltechnologies.com)

## License
[The MIT licensed](LICENSE).
