# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2020-03-13)


### Features

* Init project into NestJs Framework ([8dca70b](https://gitlab.com/febe001/digit-main-api/commit/8dca70bc6b3c23c90bce89f2d6c1f2a7618d4a41))


### Bug Fixes

* Delete package-lock.json ([85a6fc7](https://gitlab.com/febe001/digit-main-api/commit/85a6fc713d51cbb68a1bd954a8958a2eb0030d78))
* Remove deprecated package ([c9e554f](https://gitlab.com/febe001/digit-main-api/commit/c9e554f73cc7f2cf54b4548c62ebd3a15972a289))

## [0.0.1] - 2020-11-12

### Added

### Changed

### Fixed

### Removed
